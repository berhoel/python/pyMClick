"""Check Millionenklick result emails."""

from __future__ import annotations

import atexit
import email
import email.errors
import email.header
import fcntl
import mailbox
from mailbox import MHMessage
from pathlib import Path
import pickle
import re
import shutil
import stat
from typing import IO

from bs4 import BeautifulSoup
import numpy as np
from rich.console import Console
import scipy.stats as st  # type: ignore[import-untyped]

from berhoel import helper

__date__ = "2024/08/07 17:09:51 hoel"
__author__ = "`Berthold Höllmann <berthold@xn--hllmanns-n4a.de>`__"
__copyright__ = "Copyright © 2004, 2013, 2017 by Berthold Höllmann"


MAILSOURCE = Path().home() / "Mail" / "web.de.millionenklick.ZI"

SUBJECT_MATCH = re.compile(
    r"Millionenklick - Ergebnis der (?P<number>\d+)\. Ziehung", re.MULTILINE
)
SUBJECT2_MATCH = re.compile(r"Millionenklick Ziehungsergebnisse", re.MULTILINE)

DATA_MATCH = re.compile(
    r"(?:Liebe MillionenKlickerinnen und MillionenKlicker,(\n)+"
    r"bei der Ziehung des MillionenKlick am \d{2}\.\d{2}.\d{4} wurden "
    r"folgende Gewinnzahlen gezogen:?|"
    r"die \d+. Ziehung am \d{2}\.\d{2}.\d{4} ergab folgende Gewinnzahlen:)"
    r"(\n)+"
    r"(?P<res1>\d{1,2}) +(?P<res2>\d{1,2}) +(?P<res3>\d{1,2}) +"
    r"(?P<res4>\d{1,2}) +(?P<res5>\d{1,2}) +(?P<res6>\d{1,2}) *\n+"
    r"(?:Zusatz|Super)zahl: (?P<resz>\d{1,2})",
    re.MULTILINE,
)

STATEFILE_NAME = Path().home() / ".cache" / "m_click" / "m_click.statfile"

CONSOLE = Console()


def mk_dir_tree(path: Path) -> None:
    """Generate directory including missing upper directories."""
    if not path.exists():
        mode = (
            stat.S_ISGID
            | stat.S_IRWXU
            | stat.S_IRGRP
            | stat.S_IXGRP
            | stat.S_IROTH
            | stat.S_IXOTH
        )
        path.mkdir(mode=mode, parents=True)


class SaveState:
    """Save state information on already downloaded files and dates tried."""

    def __init__(self, dest: Path) -> None:
        """Initialize class instance."""
        mk_dir_tree(dest.parent)
        self.data: dict[str, tuple] = {}


def savestate(state: SaveState, statfile: IO) -> None:
    """Save state dictionary."""
    pickle.dump(state, statfile, pickle.HIGHEST_PROTOCOL)
    fcntl.fcntl(statfile, fcntl.LOCK_UN)


def read_data(  # noqa:C901,PLR0912,PLR0915
    mailsource: Path,
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Read data from mailsource."""

    def msgfactory(file_p: IO) -> MHMessage:
        """Return email content."""
        try:
            return MHMessage(email.message_from_binary_file(file_p))
        except email.errors.MessageParseError:
            # Don't return None since that will stop the mailbox
            # iterator
            CONSOLE.print(file_p.read())
            return MHMessage("")

    mbox = mailbox.MH(mailsource, msgfactory)
    res_n, res_z, res_n_old, res_z_old = [], [], [], []

    state = None
    if STATEFILE_NAME.exists():
        with STATEFILE_NAME.open("rb") as statefile:
            state = pickle.load(statefile)  # noqa:S301
        shutil.copy2(STATEFILE_NAME, f"{STATEFILE_NAME}_BAK")
    else:
        state = SaveState(STATEFILE_NAME)
    # Cannot use context handler here, file is used in atexit handler.
    statfile = Path(STATEFILE_NAME).open("wb")  # noqa:SIM115
    fcntl.fcntl(statfile, fcntl.LOCK_EX)
    atexit.register(savestate, state, statfile)

    swirl = helper.swirl(helper.SwirlSelect.DOTS)

    for msg in mbox:
        next(swirl)

        m_id = msg["message-id"]
        if m_id not in state.data:
            k = SUBJECT2_MATCH.match(
                " ".join([i[0] for i in email.header.decode_header(msg["subject"])])
            )
            if k:
                if (
                    msg.get_content_maintype() == "multipart"
                    and msg.get_content_subtype() == "alternative"
                ):
                    payload = msg.get_payload()
                    for smsg in payload:
                        if not isinstance(smsg, email.message.Message):
                            raise TypeError
                        if (
                            smsg.get_content_maintype() == "text"
                            and smsg.get_content_subtype() == "html"
                        ):
                            payload = smsg.get_payload(decode=True)
                            break
                else:
                    payload = msg.get_payload(decode=True)
                if not isinstance(payload, (str, bytes)):
                    raise ValueError
                soup = BeautifulSoup(payload, "lxml")
                values = [
                    int(img["alt"]) - 1
                    for img in soup.find_all("img")
                    if img["src"].find("mioklick_number") > 0
                ]
                z_value = next(
                    int(img["alt"]) - 1
                    for img in soup.find_all("img")
                    if img["src"].find("mioklick_super") > 0
                )
                state.data[m_id] = (None, None, values, z_value)
            else:
                j = SUBJECT_MATCH.match(
                    " ".join([i[0] for i in email.header.decode_header(msg["subject"])])
                )
                if j:
                    payload = msg.get_payload()
                    if not isinstance(payload, str):
                        raise ValueError
                    k = DATA_MATCH.search(payload)
                    if k:
                        values = [
                            int(g)
                            for g in k.group(
                                "res1", "res2", "res3", "res4", "res5", "res6", "resz"
                            )
                        ]
                        state.data[m_id] = (values[:-1], values[-1], None, None)
                    elif True:
                        emsg = "Error: No Volume data found in email"
                        raise ValueError(emsg)
                    else:
                        CONSOLE.print("ignored")
                else:
                    CONSOLE.print(
                        "Not parsed message with subject:\n!{}!".format(msg["subject"])
                    )
        old_v, old_z, new_v, new_z = state.data[m_id]
        if old_v is None:
            res_n.extend(new_v)
            res_z.append(new_z)
        else:
            res_n_old.extend(old_v)
            res_z_old.append(old_z)

    return (
        np.array(res_n, dtype=int),
        np.array(res_z, dtype=int),
        np.array(res_n_old, dtype=int),
        np.array(res_z_old, dtype=int),
    )


def print_res(n_data: np.ndarray, z_data: np.ndarray) -> None:
    """Output the result."""
    r_n = np.array([n_data[i] for i in range(len(n_data))], dtype=int)
    r_n = (np.arange(len(n_data), dtype=int) + 1)[r_n.argsort()]
    maxres = sorted(r_n[-6:])
    minres = sorted(r_n[:6])
    r_z = np.array([z_data[i] for i in range(len(z_data))], dtype=int)
    r_z = np.arange(len(z_data), dtype=int)[r_z.argsort()]
    res = (
        n_data.sum() // 6,
        *tuple(maxres),
        *tuple(minres),
        r_z[-1],
        r_z[0],
    )
    CONSOLE.print(
        """From %d results:

  Erstes Ergebnis (häufigsten Zahlen):    Zweites Ergebnis (Seltenste Zahlen):
     %2d %2d %2d %2d %2d %2d                 %2d %2d %2d %2d %2d %2d
     Zusatzzahl: %2d                         Zusatzzahl: %2d
"""
        % res
    )


def print_res_new(n_data: np.ndarray, z_data: np.ndarray) -> None:
    """Output the result."""
    number = n_data.shape[0] // 6
    n_last, z_last = n_data[:-6], z_data[:-1]
    data_n, data_z = [], None
    last_n, last_z = [], None
    val, val_last = None, None
    for _ in range(6):
        if val:
            n_data = n_data[n_data != val]
            n_last = n_last[n_last != val_last]
        v = st.mode(n_data, keepdims=True)
        val = v.mode[0]
        data_n.append(val)
        v = st.mode(n_last, keepdims=True)
        val_last = v.mode[0]
        last_n.append(val_last)
    data_n.sort()
    last_n.sort()
    while not data_z or (data_z in data_n):
        if data_z:
            z_data = z_data[z_data != data_z]
        data_z = st.mode(z_data, keepdims=True).mode[0]
    while not last_z or (last_z in last_n):
        if last_z:
            z_last = z_last[z_last != last_z]
        last_z = st.mode(z_last, keepdims=True).mode[0]
    CONSOLE.print(
        f"""From {number} results:

  Häufigsten Zahlen        Häufigsten Zahlen letzte Ziehung
     {data_n[0]:3d}{data_n[1]:3d}{data_n[2]:3d}{data_n[3]:3d}{data_n[4]:3d}\
{data_n[5]:3d}       {last_n[0]:3d}{last_n[1]:3d}{last_n[2]:3d}{last_n[3]:3d}\
{last_n[4]:3d}{last_n[5]:3d}
     Zusatzzahl:{data_z:3d}           Zusatzzahl:{last_z:3d}
"""
    )


def main() -> None:
    """Execute main program."""
    raw_n_new, raw_z_new, raw_n, raw_z = read_data(MAILSOURCE)
    print_res_new(raw_n_new, raw_z_new)


if __name__ == "__main__":
    main()
