"""Testing m_click module."""

__date__ = "2024/08/07 16:22:53 Berthold Höllmann"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2013,2022 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"
