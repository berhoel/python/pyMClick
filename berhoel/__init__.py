"""Initialization for berhoel python namespace packages."""

from pkgutil import extend_path

__date__ = "2024/08/07 17:07:56 hoel"[7:-1]
__scm_version__ = "$Revision$"[10:-1]
__author__ = "`Berthold Höllmann <berhoel@gmail.com>`__"
__copyright__ = "Copyright © 2017 by Berthold Höllmann"

__path__ = extend_path(__path__, __name__)
